'use strict';
const userGame = require('../masterdata/user_games.json')


module.exports = {
  async up (queryInterface, Sequelize) {
      const userGameDataToSeed = userGame.map((eachUserGame) => {
        return {
          email: eachUserGame.email,
          password: eachUserGame.password,
          createdAt: new Date(),
          updatedAt: new Date(),
        }
      })
      await queryInterface.bulkInsert('user_games', userGameDataToSeed, {});
    
},
  async down (queryInterface, Sequelize) {
      await queryInterface.bulkDelete('user_games', null, { truncate: true, restartIdentity: true });
  }
};
