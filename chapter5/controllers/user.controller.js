const { user_bio, user_game, user_history } = require('../models');

const getAllUserData = async (req, res) => {
    let { page, row } = req.query;
    page -= 1;
    try{
        const options = {
            attributes:['id', 'email', 'password'],
            include: [
                { 
                    model: user_bio,
                    attributes:['username', 'age', 'gender']
                },
                { 
                    model: user_history,
                    attributes:['game_played', 'time_played']
                }
            ],
            offset: page,
            limit: row,
        }
        
        const userData = await user_game.findAll(options);
        res.status(200).json({
            status: 'success',
            data: userData
        })
    }catch(err){
        console.log(err);
        throw new Error(err);
    }
}

const getUserDataById = async (req, res) => {
    //getting user data by id
    try{
        const userData = await user_game.findOne({
            where: {
                id: req.params.id
            },
            attributes:['id', 'email', 'password'],
        });
        if (!userData) {
            res.status(404).json({
                status: 'fail',
                message: 'Data tidak ditemukan'
            })
        } else {
            res.status(200).json({
                status: 'success',
                data: userData
            })
        }
    }catch(err){
        console.log(err);
        throw new Error(err);
    }
}

const createUserGame = async (req, res) => {
    try{
        const createdUserGame = await user_game.create({
            email: req.body.email,
            password: req.body.password
        });
        if(createdUserGame) {
            await user_bio.create({
                username: req.body.username,
                age: req.body.age,
                gender: req.body.gender,
                user_id: createdUserGame.id
            }),
            await user_history.create({
                game_played: req.body.game_played,
                time_played: req.body.time_played,
                user_id: createdUserGame.id
            })
        };

        const clearCreate = await user_game.findOne({ 
            where: { 
                id: createdUserGame.id
            },
            include: [{ 
                model: user_bio,
                model: user_history
            }]
        });
        res.status(200).json({ 
            status: 'success',
            data: clearCreate
        })
    } catch(err){
        console.log(err);
    }
}
//still confusing on update feature
const updateUserGame = async (req, res) => {
    try{
        // const whereClause = { where: {id: req.params.id} }
        const updateUser = await user_game.update({
            email: req.body.email,
            password: req.body.password,
            updatedAt: new Date()
        }, {where: {id: req.params.id} })
        
        // console.log(updateUser.id);
        if(updateUser) {
            await user_bio.update({
                username: req.body.username,
                age: req.body.age,
                gender: req.body.gender,
                updatedAt: new Date()
            }, {where: {id: req.params.id}}),

            await user_history.update({
                game_played: req.body.game_played,
                time_played: req.body.time_played,
                updatedAt: new Date()
            }, {where: {id: req.params.id}})
        };
        const clearUpdate = await user_game.findOne({ 
            where: { 
                id: req.params.id
            },
            include: [{ 
                model: user_bio,
                model: user_history
            }]
        });
        res.status(200).json({ 
            status: 'success',
            data: clearUpdate
        })
    } catch(err){
        console.log(err);
    }
}

const deleteUserGame = async (req, res) => {
    try{
        const deleteUserGame = await user_game.destroy({
            where: {
                id: req.params.id
            }
        });
        if(deleteUserGame) {
            await user_bio.destroy({
                where: {
                    user_id: req.params.id
                }
            }),
            await user_history.destroy({
                where: {
                    user_id: req.params.id
                }
            })
        };
        res.status(200).json({ 
            status: 'success',
            message: 'Data berhasil dihapus'
        })
    } catch(err){
        console.log(err);
    }
}

module.exports = {
    getAllUserData,
    getUserDataById,
    createUserGame,
    updateUserGame,
    deleteUserGame
}