const { user_history } = require('../models');

const getUserHistoryData = async (req, res) => {
    //getting user data by id
    let { page, row } = req.query;
    page -= 1;
    try{
        const options = {
            attributes:['id', 'game_played', 'time_played'],
            offset: page,
            limit: row,
        }
        const userData = await user_history.findAll(options);
        res.status(200).json({
            status: 'success',
            data: userData
        })   
    }catch(err){
        console.log(err);
        throw new Error(err);
    }
}

const getUserHistoryById = async (req, res) => {
    try{
        const userHistory = await user_history.findOne({
            where: {
                id: req.params.id
            },
            attributes:['id', 'game_played', 'time_played'],
        });
        if (!userHistory) {
            res.status(404).json({
                status: 'fail',
                message: 'Data tidak ditemukan'
            })
        } else {
            res.status(200).json({
                status: 'success',
                data: userHistory
            })
        }
    }catch(err){
        console.log(err);
        throw new Error(err);
    }
}

const updateUserHistoryData = async (req, res) => {
    const { game_played, time_played } = req.body;
    try{
        const updatedUserHistory = await user_history.update({
            game_played,
            time_played
        }, {
            where: {
                id: req.params.id
            }
        });
        res.status(200).json({
            status: 'success',
            data: updatedUserHistory
        })
    }catch(err){
        res.status(500).json({
            status: 'fail',
            message: err.message
        })
    }
}

const deleteUserHistoryData = async (req, res) => {
    try{
        const deletedUserHistory = await user_history.destroy({
            where: {
                id: req.params.id
            }
        });
        res.status(200).json({
            status: 'success',
            data: deletedUserHistory
        })
    }catch(err){
        console.log(err);
        throw new Error(err);
    }
}

module.exports = {
    getUserHistoryData,
    getUserHistoryById,
    updateUserHistoryData,
    deleteUserHistoryData
}