require('dotenv').config();
const express = require('express');
const router = require('./routes/index.route');
const app = express();
const swaggerJSON = require('./documentation/swagger.json');
const swaggerUI = require('swagger-ui-express');
const port = process.env.PORT;

app.use('/docs', swaggerUI.serve, swaggerUI.setup(swaggerJSON));

app.use(express.json());
app.use(express.urlencoded({ extended: true }));

app.use(`${process.env.BASE_URL}`, router); //  /workspace

app.use((err, req, res, next) => {
    if (err === 'Data tidak ada') {
        res.status(400).json({
            status: 'Bad Request',
            message: err
        })
    } else if (err === `Bad for Creating`) {
        res.status(400).json({
            status: 'Not Found',
            message: err.message
        });
    } 
    else {
        res.status(500).json({
            status: err.name,
            message: err.message
        });
    }

})

app.listen(port, () => {
    console.log(`I'm Listening to the port ${port}`);
});