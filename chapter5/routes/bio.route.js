const express = require('express');
const router = express.Router();
const { getUserBioData, getUserBioDataById ,updateUserBioData, deleteUserBioData } = require('../controllers/bio.controller')

router.get('/', getUserBioData);
router.get('/:id', getUserBioDataById);
router.put('/:id', updateUserBioData);
router.delete('/:id', deleteUserBioData);

module.exports = router;