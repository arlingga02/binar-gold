const express = require('express');
const router = express.Router();
const { getUserHistoryData, getUserHistoryById, updateUserHistoryData, deleteUserHistoryData } = require('../controllers/history.controller')

router.get('/', getUserHistoryData);
router.get('/:id', getUserHistoryById);
router.put('/:id', updateUserHistoryData);
router.delete('/:id', deleteUserHistoryData);

module.exports = router;
