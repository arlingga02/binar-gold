const express = require('express');
const router = express.Router();
const { getAllUserData, createUserGame, getUserDataById, updateUserGame, deleteUserGame } = require('../controllers/user.controller')


router.get('/', getAllUserData);
router.get('/:id', getUserDataById);
router.post('/', createUserGame);
router.put('/:id', updateUserGame);
router.delete('/:id', deleteUserGame);

module.exports = router;