const express = require('express');
const router = express.Router();
const { validate } = require('../misc/validation')
const userRoutes = require('./user.route');
const histRoutes = require('./history.route');
const bioRoutes = require('./bio.route');

router.use('/user', userRoutes);
router.use('/history', histRoutes);
router.use('/bio', bioRoutes);

module.exports = router;